<?php

return [
    'kushi.kushi1s' => [
        'index' => 'kushi::kushi1s.list resource',
        'create' => 'kushi::kushi1s.create resource',
        'edit' => 'kushi::kushi1s.edit resource',
        'destroy' => 'kushi::kushi1s.destroy resource',
    ],
    'kushi.kushi2s' => [
        'index' => 'kushi::kushi2s.list resource',
        'create' => 'kushi::kushi2s.create resource',
        'edit' => 'kushi::kushi2s.edit resource',
        'destroy' => 'kushi::kushi2s.destroy resource',
    ],
// append


];
