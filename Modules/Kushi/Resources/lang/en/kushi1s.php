<?php

return [
    'list resource' => 'List kushi1s',
    'create resource' => 'Create kushi1s',
    'edit resource' => 'Edit kushi1s',
    'destroy resource' => 'Destroy kushi1s',
    'title' => [
        'kushi1s' => 'Kushi1',
        'create kushi1' => 'Create a kushi1',
        'edit kushi1' => 'Edit a kushi1',
    ],
    'button' => [
        'create kushi1' => 'Create a kushi1',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
