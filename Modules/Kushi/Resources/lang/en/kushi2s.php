<?php

return [
    'list resource' => 'List kushi2s',
    'create resource' => 'Create kushi2s',
    'edit resource' => 'Edit kushi2s',
    'destroy resource' => 'Destroy kushi2s',
    'title' => [
        'kushi2s' => 'Kushi2',
        'create kushi2' => 'Create a kushi2',
        'edit kushi2' => 'Edit a kushi2',
    ],
    'button' => [
        'create kushi2' => 'Create a kushi2',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
