<?php

namespace Modules\Kushi\Listeners;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterKushiSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('kushi::kushis.title.kushis'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('kushi::kushi1s.title.kushi1s'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.kushi.kushi1.create');
                    $item->route('admin.kushi.kushi1.index');
                    $item->authorize(
                        $this->auth->hasAccess('kushi.kushi1s.index')
                    );
                });
                $item->item(trans('kushi::kushi2s.title.kushi2s'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.kushi.kushi2.create');
                    $item->route('admin.kushi.kushi2.index');
                    $item->authorize(
                        $this->auth->hasAccess('kushi.kushi2s.index')
                    );
                });
// append


            });
        });

        return $menu;
    }
}
