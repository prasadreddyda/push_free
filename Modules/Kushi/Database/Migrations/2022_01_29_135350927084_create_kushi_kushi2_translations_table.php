<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKushiKushi2TranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kushi__kushi2_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('kushi2_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['kushi2_id', 'locale']);
            $table->foreign('kushi2_id')->references('id')->on('kushi__kushi2s')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kushi__kushi2_translations', function (Blueprint $table) {
            $table->dropForeign(['kushi2_id']);
        });
        Schema::dropIfExists('kushi__kushi2_translations');
    }
}
