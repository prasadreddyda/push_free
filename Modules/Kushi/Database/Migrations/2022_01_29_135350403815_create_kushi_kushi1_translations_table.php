<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKushiKushi1TranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kushi__kushi1_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('kushi1_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['kushi1_id', 'locale']);
            $table->foreign('kushi1_id')->references('id')->on('kushi__kushi1s')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kushi__kushi1_translations', function (Blueprint $table) {
            $table->dropForeign(['kushi1_id']);
        });
        Schema::dropIfExists('kushi__kushi1_translations');
    }
}
