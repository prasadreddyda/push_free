<?php

namespace Modules\Kushi\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Kushi\Entities\Kushi2;
use Modules\Kushi\Http\Requests\CreateKushi2Request;
use Modules\Kushi\Http\Requests\UpdateKushi2Request;
use Modules\Kushi\Repositories\Kushi2Repository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class Kushi2Controller extends AdminBaseController
{
    /**
     * @var Kushi2Repository
     */
    private $kushi2;

    public function __construct(Kushi2Repository $kushi2)
    {
        parent::__construct();

        $this->kushi2 = $kushi2;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$kushi2s = $this->kushi2->all();

        return view('kushi::admin.kushi2s.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('kushi::admin.kushi2s.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateKushi2Request $request
     * @return Response
     */
    public function store(CreateKushi2Request $request)
    {
        $this->kushi2->create($request->all());

        return redirect()->route('admin.kushi.kushi2.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('kushi::kushi2s.title.kushi2s')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Kushi2 $kushi2
     * @return Response
     */
    public function edit(Kushi2 $kushi2)
    {
        return view('kushi::admin.kushi2s.edit', compact('kushi2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Kushi2 $kushi2
     * @param  UpdateKushi2Request $request
     * @return Response
     */
    public function update(Kushi2 $kushi2, UpdateKushi2Request $request)
    {
        $this->kushi2->update($kushi2, $request->all());

        return redirect()->route('admin.kushi.kushi2.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('kushi::kushi2s.title.kushi2s')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Kushi2 $kushi2
     * @return Response
     */
    public function destroy(Kushi2 $kushi2)
    {
        $this->kushi2->destroy($kushi2);

        return redirect()->route('admin.kushi.kushi2.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('kushi::kushi2s.title.kushi2s')]));
    }
}
