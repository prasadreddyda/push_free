<?php

namespace Modules\Kushi\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Kushi\Entities\Kushi1;
use Modules\Kushi\Http\Requests\CreateKushi1Request;
use Modules\Kushi\Http\Requests\UpdateKushi1Request;
use Modules\Kushi\Repositories\Kushi1Repository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class Kushi1Controller extends AdminBaseController
{
    /**
     * @var Kushi1Repository
     */
    private $kushi1;

    public function __construct(Kushi1Repository $kushi1)
    {
        parent::__construct();

        $this->kushi1 = $kushi1;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$kushi1s = $this->kushi1->all();

        return view('kushi::admin.kushi1s.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('kushi::admin.kushi1s.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateKushi1Request $request
     * @return Response
     */
    public function store(CreateKushi1Request $request)
    {
        $this->kushi1->create($request->all());

        return redirect()->route('admin.kushi.kushi1.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('kushi::kushi1s.title.kushi1s')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Kushi1 $kushi1
     * @return Response
     */
    public function edit(Kushi1 $kushi1)
    {
        return view('kushi::admin.kushi1s.edit', compact('kushi1'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Kushi1 $kushi1
     * @param  UpdateKushi1Request $request
     * @return Response
     */
    public function update(Kushi1 $kushi1, UpdateKushi1Request $request)
    {
        $this->kushi1->update($kushi1, $request->all());

        return redirect()->route('admin.kushi.kushi1.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('kushi::kushi1s.title.kushi1s')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Kushi1 $kushi1
     * @return Response
     */
    public function destroy(Kushi1 $kushi1)
    {
        $this->kushi1->destroy($kushi1);

        return redirect()->route('admin.kushi.kushi1.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('kushi::kushi1s.title.kushi1s')]));
    }
}
