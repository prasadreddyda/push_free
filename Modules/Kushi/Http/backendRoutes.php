<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/kushi'], function (Router $router) {
    $router->bind('kushi1', function ($id) {
        return app('Modules\Kushi\Repositories\Kushi1Repository')->find($id);
    });
    $router->get('kushi1s', [
        'as' => 'admin.kushi.kushi1.index',
        'uses' => 'Kushi1Controller@index',
        'middleware' => 'can:kushi.kushi1s.index'
    ]);
    $router->get('kushi1s/create', [
        'as' => 'admin.kushi.kushi1.create',
        'uses' => 'Kushi1Controller@create',
        'middleware' => 'can:kushi.kushi1s.create'
    ]);
    $router->post('kushi1s', [
        'as' => 'admin.kushi.kushi1.store',
        'uses' => 'Kushi1Controller@store',
        'middleware' => 'can:kushi.kushi1s.create'
    ]);
    $router->get('kushi1s/{kushi1}/edit', [
        'as' => 'admin.kushi.kushi1.edit',
        'uses' => 'Kushi1Controller@edit',
        'middleware' => 'can:kushi.kushi1s.edit'
    ]);
    $router->put('kushi1s/{kushi1}', [
        'as' => 'admin.kushi.kushi1.update',
        'uses' => 'Kushi1Controller@update',
        'middleware' => 'can:kushi.kushi1s.edit'
    ]);
    $router->delete('kushi1s/{kushi1}', [
        'as' => 'admin.kushi.kushi1.destroy',
        'uses' => 'Kushi1Controller@destroy',
        'middleware' => 'can:kushi.kushi1s.destroy'
    ]);
    $router->bind('kushi2', function ($id) {
        return app('Modules\Kushi\Repositories\Kushi2Repository')->find($id);
    });
    $router->get('kushi2s', [
        'as' => 'admin.kushi.kushi2.index',
        'uses' => 'Kushi2Controller@index',
        'middleware' => 'can:kushi.kushi2s.index'
    ]);
    $router->get('kushi2s/create', [
        'as' => 'admin.kushi.kushi2.create',
        'uses' => 'Kushi2Controller@create',
        'middleware' => 'can:kushi.kushi2s.create'
    ]);
    $router->post('kushi2s', [
        'as' => 'admin.kushi.kushi2.store',
        'uses' => 'Kushi2Controller@store',
        'middleware' => 'can:kushi.kushi2s.create'
    ]);
    $router->get('kushi2s/{kushi2}/edit', [
        'as' => 'admin.kushi.kushi2.edit',
        'uses' => 'Kushi2Controller@edit',
        'middleware' => 'can:kushi.kushi2s.edit'
    ]);
    $router->put('kushi2s/{kushi2}', [
        'as' => 'admin.kushi.kushi2.update',
        'uses' => 'Kushi2Controller@update',
        'middleware' => 'can:kushi.kushi2s.edit'
    ]);
    $router->delete('kushi2s/{kushi2}', [
        'as' => 'admin.kushi.kushi2.destroy',
        'uses' => 'Kushi2Controller@destroy',
        'middleware' => 'can:kushi.kushi2s.destroy'
    ]);
// append


});
