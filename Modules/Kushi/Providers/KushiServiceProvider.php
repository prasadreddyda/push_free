<?php

namespace Modules\Kushi\Providers;

use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Kushi\Listeners\RegisterKushiSidebar;

class KushiServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterKushiSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('kushi1s', array_dot(trans('kushi::kushi1s')));
            $event->load('kushi2s', array_dot(trans('kushi::kushi2s')));
            // append translations


        });


    }

    public function boot()
    {
        $this->publishConfig('kushi', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Kushi\Repositories\Kushi1Repository',
            function () {
                $repository = new \Modules\Kushi\Repositories\Eloquent\EloquentKushi1Repository(new \Modules\Kushi\Entities\Kushi1());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Kushi\Repositories\Cache\CacheKushi1Decorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Kushi\Repositories\Kushi2Repository',
            function () {
                $repository = new \Modules\Kushi\Repositories\Eloquent\EloquentKushi2Repository(new \Modules\Kushi\Entities\Kushi2());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Kushi\Repositories\Cache\CacheKushi2Decorator($repository);
            }
        );
// add bindings


    }


}
