<?php

namespace Modules\Kushi\Repositories\Eloquent;

use Modules\Kushi\Repositories\Kushi2Repository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentKushi2Repository extends EloquentBaseRepository implements Kushi2Repository
{
}
