<?php

namespace Modules\Kushi\Repositories\Eloquent;

use Modules\Kushi\Repositories\Kushi1Repository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentKushi1Repository extends EloquentBaseRepository implements Kushi1Repository
{
}
