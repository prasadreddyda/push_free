<?php

namespace Modules\Kushi\Repositories\Cache;

use Modules\Kushi\Repositories\Kushi2Repository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheKushi2Decorator extends BaseCacheDecorator implements Kushi2Repository
{
    public function __construct(Kushi2Repository $kushi2)
    {
        parent::__construct();
        $this->entityName = 'kushi.kushi2s';
        $this->repository = $kushi2;
    }
}
