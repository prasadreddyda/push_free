<?php

namespace Modules\Kushi\Repositories\Cache;

use Modules\Kushi\Repositories\Kushi1Repository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheKushi1Decorator extends BaseCacheDecorator implements Kushi1Repository
{
    public function __construct(Kushi1Repository $kushi1)
    {
        parent::__construct();
        $this->entityName = 'kushi.kushi1s';
        $this->repository = $kushi1;
    }
}
