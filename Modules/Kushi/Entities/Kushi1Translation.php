<?php

namespace Modules\Kushi\Entities;

use Illuminate\Database\Eloquent\Model;

class Kushi1Translation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'kushi__kushi1_translations';
}
