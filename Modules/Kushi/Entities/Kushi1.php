<?php

namespace Modules\Kushi\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Kushi1 extends Model
{
    use Translatable;

    protected $table = 'kushi__kushi1s';
    public $translatedAttributes = [];
    protected $fillable = [];
}
