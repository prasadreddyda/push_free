<?php

namespace Modules\Kushi\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Kushi2 extends Model
{
    use Translatable;

    protected $table = 'kushi__kushi2s';
    public $translatedAttributes = [];
    protected $fillable = [];
}
