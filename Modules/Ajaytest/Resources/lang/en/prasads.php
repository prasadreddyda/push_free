<?php

return [
    'list resource' => 'List prasads',
    'create resource' => 'Create prasads',
    'edit resource' => 'Edit prasads',
    'destroy resource' => 'Destroy prasads',
    'title' => [
        'prasads' => 'Prasad',
        'create prasad' => 'Create a prasad',
        'edit prasad' => 'Edit a prasad',
    ],
    'button' => [
        'create prasad' => 'Create a prasad',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
