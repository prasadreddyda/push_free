<?php

return [
    'list resource' => 'List ajayvs',
    'create resource' => 'Create ajayvs',
    'edit resource' => 'Edit ajayvs',
    'destroy resource' => 'Destroy ajayvs',
    'title' => [
        'ajayvs' => 'Ajayv',
        'create ajayv' => 'Create a ajayv',
        'edit ajayv' => 'Edit a ajayv',
    ],
    'button' => [
        'create ajayv' => 'Create a ajayv',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
