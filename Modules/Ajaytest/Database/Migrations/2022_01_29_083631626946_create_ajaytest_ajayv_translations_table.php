<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAjaytestAjayvTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ajaytest__ajayv_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('ajayv_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['ajayv_id', 'locale']);
            $table->foreign('ajayv_id')->references('id')->on('ajaytest__ajayvs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ajaytest__ajayv_translations', function (Blueprint $table) {
            $table->dropForeign(['ajayv_id']);
        });
        Schema::dropIfExists('ajaytest__ajayv_translations');
    }
}
