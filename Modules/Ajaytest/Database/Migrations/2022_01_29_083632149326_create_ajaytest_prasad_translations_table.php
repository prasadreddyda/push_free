<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAjaytestPrasadTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ajaytest__prasad_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('prasad_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['prasad_id', 'locale']);
            $table->foreign('prasad_id')->references('id')->on('ajaytest__prasads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ajaytest__prasad_translations', function (Blueprint $table) {
            $table->dropForeign(['prasad_id']);
        });
        Schema::dropIfExists('ajaytest__prasad_translations');
    }
}
