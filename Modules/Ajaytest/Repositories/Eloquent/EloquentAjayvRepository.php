<?php

namespace Modules\Ajaytest\Repositories\Eloquent;

use Modules\Ajaytest\Repositories\AjayvRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentAjayvRepository extends EloquentBaseRepository implements AjayvRepository
{
}
