<?php

namespace Modules\Ajaytest\Repositories\Eloquent;

use Modules\Ajaytest\Repositories\PrasadRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPrasadRepository extends EloquentBaseRepository implements PrasadRepository
{
}
