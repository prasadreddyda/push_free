<?php

namespace Modules\Ajaytest\Repositories\Cache;

use Modules\Ajaytest\Repositories\AjayvRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheAjayvDecorator extends BaseCacheDecorator implements AjayvRepository
{
    public function __construct(AjayvRepository $ajayv)
    {
        parent::__construct();
        $this->entityName = 'ajaytest.ajayvs';
        $this->repository = $ajayv;
    }
}
