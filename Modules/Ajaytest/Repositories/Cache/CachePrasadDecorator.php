<?php

namespace Modules\Ajaytest\Repositories\Cache;

use Modules\Ajaytest\Repositories\PrasadRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePrasadDecorator extends BaseCacheDecorator implements PrasadRepository
{
    public function __construct(PrasadRepository $prasad)
    {
        parent::__construct();
        $this->entityName = 'ajaytest.prasads';
        $this->repository = $prasad;
    }
}
