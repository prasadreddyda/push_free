<?php

namespace Modules\Ajaytest\Providers;

use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Ajaytest\Listeners\RegisterAjaytestSidebar;

class AjaytestServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterAjaytestSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('ajayvs', array_dot(trans('ajaytest::ajayvs')));
            $event->load('prasads', array_dot(trans('ajaytest::prasads')));
            // append translations


        });


    }

    public function boot()
    {
        $this->publishConfig('ajaytest', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Ajaytest\Repositories\AjayvRepository',
            function () {
                $repository = new \Modules\Ajaytest\Repositories\Eloquent\EloquentAjayvRepository(new \Modules\Ajaytest\Entities\Ajayv());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Ajaytest\Repositories\Cache\CacheAjayvDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Ajaytest\Repositories\PrasadRepository',
            function () {
                $repository = new \Modules\Ajaytest\Repositories\Eloquent\EloquentPrasadRepository(new \Modules\Ajaytest\Entities\Prasad());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Ajaytest\Repositories\Cache\CachePrasadDecorator($repository);
            }
        );
// add bindings


    }


}
