<?php

namespace Modules\Ajaytest\Entities;

use Illuminate\Database\Eloquent\Model;

class PrasadTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'ajaytest__prasad_translations';
}
