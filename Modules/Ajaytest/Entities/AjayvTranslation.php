<?php

namespace Modules\Ajaytest\Entities;

use Illuminate\Database\Eloquent\Model;

class AjayvTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'ajaytest__ajayv_translations';
}
