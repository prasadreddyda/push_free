<?php

namespace Modules\Ajaytest\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Prasad extends Model
{
    use Translatable;

    protected $table = 'ajaytest__prasads';
    public $translatedAttributes = [];
    protected $fillable = [];
}
