<?php

namespace Modules\Ajaytest\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Ajayv extends Model
{
    use Translatable;

    protected $table = 'ajaytest__ajayvs';
    public $translatedAttributes = [];
    protected $fillable = [];
}
