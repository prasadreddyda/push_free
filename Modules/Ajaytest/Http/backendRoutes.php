<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/ajaytest'], function (Router $router) {
    $router->bind('ajayv', function ($id) {
        return app('Modules\Ajaytest\Repositories\AjayvRepository')->find($id);
    });
    $router->get('ajayvs', [
        'as' => 'admin.ajaytest.ajayv.index',
        'uses' => 'AjayvController@index',
        'middleware' => 'can:ajaytest.ajayvs.index'
    ]);
    $router->get('ajayvs/create', [
        'as' => 'admin.ajaytest.ajayv.create',
        'uses' => 'AjayvController@create',
        'middleware' => 'can:ajaytest.ajayvs.create'
    ]);
    $router->post('ajayvs', [
        'as' => 'admin.ajaytest.ajayv.store',
        'uses' => 'AjayvController@store',
        'middleware' => 'can:ajaytest.ajayvs.create'
    ]);
    $router->get('ajayvs/{ajayv}/edit', [
        'as' => 'admin.ajaytest.ajayv.edit',
        'uses' => 'AjayvController@edit',
        'middleware' => 'can:ajaytest.ajayvs.edit'
    ]);
    $router->put('ajayvs/{ajayv}', [
        'as' => 'admin.ajaytest.ajayv.update',
        'uses' => 'AjayvController@update',
        'middleware' => 'can:ajaytest.ajayvs.edit'
    ]);
    $router->delete('ajayvs/{ajayv}', [
        'as' => 'admin.ajaytest.ajayv.destroy',
        'uses' => 'AjayvController@destroy',
        'middleware' => 'can:ajaytest.ajayvs.destroy'
    ]);
    $router->bind('prasad', function ($id) {
        return app('Modules\Ajaytest\Repositories\PrasadRepository')->find($id);
    });
    $router->get('prasads', [
        'as' => 'admin.ajaytest.prasad.index',
        'uses' => 'PrasadController@index',
        'middleware' => 'can:ajaytest.prasads.index'
    ]);
    $router->get('prasads/create', [
        'as' => 'admin.ajaytest.prasad.create',
        'uses' => 'PrasadController@create',
        'middleware' => 'can:ajaytest.prasads.create'
    ]);
    $router->post('prasads', [
        'as' => 'admin.ajaytest.prasad.store',
        'uses' => 'PrasadController@store',
        'middleware' => 'can:ajaytest.prasads.create'
    ]);
    $router->get('prasads/{prasad}/edit', [
        'as' => 'admin.ajaytest.prasad.edit',
        'uses' => 'PrasadController@edit',
        'middleware' => 'can:ajaytest.prasads.edit'
    ]);
    $router->put('prasads/{prasad}', [
        'as' => 'admin.ajaytest.prasad.update',
        'uses' => 'PrasadController@update',
        'middleware' => 'can:ajaytest.prasads.edit'
    ]);
    $router->delete('prasads/{prasad}', [
        'as' => 'admin.ajaytest.prasad.destroy',
        'uses' => 'PrasadController@destroy',
        'middleware' => 'can:ajaytest.prasads.destroy'
    ]);
// append


});
