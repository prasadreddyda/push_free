<?php

namespace Modules\Ajaytest\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Ajaytest\Entities\Ajayv;
use Modules\Ajaytest\Http\Requests\CreateAjayvRequest;
use Modules\Ajaytest\Http\Requests\UpdateAjayvRequest;
use Modules\Ajaytest\Repositories\AjayvRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class AjayvController extends AdminBaseController
{
    /**
     * @var AjayvRepository
     */
    private $ajayv;

    public function __construct(AjayvRepository $ajayv)
    {
        parent::__construct();

        $this->ajayv = $ajayv;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$ajayvs = $this->ajayv->all();

        return view('ajaytest::admin.ajayvs.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('ajaytest::admin.ajayvs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateAjayvRequest $request
     * @return Response
     */
    public function store(CreateAjayvRequest $request)
    {
        $this->ajayv->create($request->all());

        return redirect()->route('admin.ajaytest.ajayv.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('ajaytest::ajayvs.title.ajayvs')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Ajayv $ajayv
     * @return Response
     */
    public function edit(Ajayv $ajayv)
    {
        return view('ajaytest::admin.ajayvs.edit', compact('ajayv'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Ajayv $ajayv
     * @param  UpdateAjayvRequest $request
     * @return Response
     */
    public function update(Ajayv $ajayv, UpdateAjayvRequest $request)
    {
        $this->ajayv->update($ajayv, $request->all());

        return redirect()->route('admin.ajaytest.ajayv.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('ajaytest::ajayvs.title.ajayvs')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Ajayv $ajayv
     * @return Response
     */
    public function destroy(Ajayv $ajayv)
    {
        $this->ajayv->destroy($ajayv);

        return redirect()->route('admin.ajaytest.ajayv.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('ajaytest::ajayvs.title.ajayvs')]));
    }
}
