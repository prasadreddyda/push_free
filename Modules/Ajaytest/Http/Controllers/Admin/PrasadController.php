<?php

namespace Modules\Ajaytest\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Ajaytest\Entities\Prasad;
use Modules\Ajaytest\Http\Requests\CreatePrasadRequest;
use Modules\Ajaytest\Http\Requests\UpdatePrasadRequest;
use Modules\Ajaytest\Repositories\PrasadRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class PrasadController extends AdminBaseController
{
    /**
     * @var PrasadRepository
     */
    private $prasad;

    public function __construct(PrasadRepository $prasad)
    {
        parent::__construct();

        $this->prasad = $prasad;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$prasads = $this->prasad->all();

        return view('ajaytest::admin.prasads.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('ajaytest::admin.prasads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePrasadRequest $request
     * @return Response
     */
    public function store(CreatePrasadRequest $request)
    {
        $this->prasad->create($request->all());

        return redirect()->route('admin.ajaytest.prasad.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('ajaytest::prasads.title.prasads')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Prasad $prasad
     * @return Response
     */
    public function edit(Prasad $prasad)
    {
        return view('ajaytest::admin.prasads.edit', compact('prasad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Prasad $prasad
     * @param  UpdatePrasadRequest $request
     * @return Response
     */
    public function update(Prasad $prasad, UpdatePrasadRequest $request)
    {
        $this->prasad->update($prasad, $request->all());

        return redirect()->route('admin.ajaytest.prasad.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('ajaytest::prasads.title.prasads')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Prasad $prasad
     * @return Response
     */
    public function destroy(Prasad $prasad)
    {
        $this->prasad->destroy($prasad);

        return redirect()->route('admin.ajaytest.prasad.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('ajaytest::prasads.title.prasads')]));
    }
}
