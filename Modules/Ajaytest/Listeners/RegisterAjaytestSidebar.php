<?php

namespace Modules\Ajaytest\Listeners;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterAjaytestSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('ajaytest::ajaytests.title.ajaytests'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('ajaytest::ajayvs.title.ajayvs'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.ajaytest.ajayv.create');
                    $item->route('admin.ajaytest.ajayv.index');
                    $item->authorize(
                        $this->auth->hasAccess('ajaytest.ajayvs.index')
                    );
                });
                $item->item(trans('ajaytest::prasads.title.prasads'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.ajaytest.prasad.create');
                    $item->route('admin.ajaytest.prasad.index');
                    $item->authorize(
                        $this->auth->hasAccess('ajaytest.prasads.index')
                    );
                });
// append


            });
        });

        return $menu;
    }
}
