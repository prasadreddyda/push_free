<?php

return [
    'finaltest.pushclients' => [
        'index' => 'finaltest::pushclients.list resource',
        'create' => 'finaltest::pushclients.create resource',
        'edit' => 'finaltest::pushclients.edit resource',
        'destroy' => 'finaltest::pushclients.destroy resource',
    ],
    'finaltest.pullclients' => [
        'index' => 'finaltest::pullclients.list resource',
        'create' => 'finaltest::pullclients.create resource',
        'edit' => 'finaltest::pullclients.edit resource',
        'destroy' => 'finaltest::pullclients.destroy resource',
    ],
// append


];
