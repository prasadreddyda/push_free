<?php

namespace Modules\Finaltest\Entities;

use Illuminate\Database\Eloquent\Model;

class PullClientsTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'finaltest__pullclients_translations';
}
