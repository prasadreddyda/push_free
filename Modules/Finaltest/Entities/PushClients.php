<?php

namespace Modules\Finaltest\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class PushClients extends Model
{
    use Translatable;

    protected $table = 'finaltest__pushclients';
    public $translatedAttributes = [];
    protected $fillable = [];
}
