<?php

namespace Modules\Finaltest\Entities;

use Illuminate\Database\Eloquent\Model;

class PushClientsTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'finaltest__pushclients_translations';
}
