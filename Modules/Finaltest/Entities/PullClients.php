<?php

namespace Modules\Finaltest\Entities;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class PullClients extends Model
{
    use Translatable;

    protected $table = 'finaltest__pullclients';
    public $translatedAttributes = [];
    protected $fillable = [];
}
