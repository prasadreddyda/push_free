<?php

namespace Modules\Finaltest\Providers;

use Illuminate\Database\Eloquent\Factory as EloquentFactory;
use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Finaltest\Listeners\RegisterFinaltestSidebar;

class FinaltestServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterFinaltestSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('pushclients', array_dot(trans('finaltest::pushclients')));
            $event->load('pullclients', array_dot(trans('finaltest::pullclients')));
            // append translations


        });


    }

    public function boot()
    {
        $this->publishConfig('finaltest', 'permissions');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Finaltest\Repositories\PushClientsRepository',
            function () {
                $repository = new \Modules\Finaltest\Repositories\Eloquent\EloquentPushClientsRepository(new \Modules\Finaltest\Entities\PushClients());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Finaltest\Repositories\Cache\CachePushClientsDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Finaltest\Repositories\PullClientsRepository',
            function () {
                $repository = new \Modules\Finaltest\Repositories\Eloquent\EloquentPullClientsRepository(new \Modules\Finaltest\Entities\PullClients());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Finaltest\Repositories\Cache\CachePullClientsDecorator($repository);
            }
        );
// add bindings


    }


}
