<?php

namespace Modules\Finaltest\Repositories\Cache;

use Modules\Finaltest\Repositories\PullClientsRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePullClientsDecorator extends BaseCacheDecorator implements PullClientsRepository
{
    public function __construct(PullClientsRepository $pullclients)
    {
        parent::__construct();
        $this->entityName = 'finaltest.pullclients';
        $this->repository = $pullclients;
    }
}
