<?php

namespace Modules\Finaltest\Repositories\Cache;

use Modules\Finaltest\Repositories\PushClientsRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePushClientsDecorator extends BaseCacheDecorator implements PushClientsRepository
{
    public function __construct(PushClientsRepository $pushclients)
    {
        parent::__construct();
        $this->entityName = 'finaltest.pushclients';
        $this->repository = $pushclients;
    }
}
