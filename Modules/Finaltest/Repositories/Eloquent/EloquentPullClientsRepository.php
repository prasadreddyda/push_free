<?php

namespace Modules\Finaltest\Repositories\Eloquent;

use Modules\Finaltest\Repositories\PullClientsRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPullClientsRepository extends EloquentBaseRepository implements PullClientsRepository
{
}
