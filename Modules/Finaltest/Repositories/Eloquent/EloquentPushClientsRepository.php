<?php

namespace Modules\Finaltest\Repositories\Eloquent;

use Modules\Finaltest\Repositories\PushClientsRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPushClientsRepository extends EloquentBaseRepository implements PushClientsRepository
{
}
