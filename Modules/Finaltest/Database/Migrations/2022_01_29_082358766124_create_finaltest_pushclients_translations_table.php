<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinaltestPushClientsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finaltest__pushclients_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('pushclients_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['pushclients_id', 'locale']);
            $table->foreign('pushclients_id')->references('id')->on('finaltest__pushclients')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('finaltest__pushclients_translations', function (Blueprint $table) {
            $table->dropForeign(['pushclients_id']);
        });
        Schema::dropIfExists('finaltest__pushclients_translations');
    }
}
