<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinaltestPullClientsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finaltest__pullclients_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('pullclients_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['pullclients_id', 'locale']);
            $table->foreign('pullclients_id')->references('id')->on('finaltest__pullclients')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('finaltest__pullclients_translations', function (Blueprint $table) {
            $table->dropForeign(['pullclients_id']);
        });
        Schema::dropIfExists('finaltest__pullclients_translations');
    }
}
