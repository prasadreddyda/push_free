<?php

return [
    'list resource' => 'List pushclients',
    'create resource' => 'Create pushclients',
    'edit resource' => 'Edit pushclients',
    'destroy resource' => 'Destroy pushclients',
    'title' => [
        'pushclients' => 'PushClients',
        'create pushclients' => 'Create a pushclients',
        'edit pushclients' => 'Edit a pushclients',
    ],
    'button' => [
        'create pushclients' => 'Create a pushclients',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
