<?php

return [
    'list resource' => 'List pullclients',
    'create resource' => 'Create pullclients',
    'edit resource' => 'Edit pullclients',
    'destroy resource' => 'Destroy pullclients',
    'title' => [
        'pullclients' => 'PullClients',
        'create pullclients' => 'Create a pullclients',
        'edit pullclients' => 'Edit a pullclients',
    ],
    'button' => [
        'create pullclients' => 'Create a pullclients',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
