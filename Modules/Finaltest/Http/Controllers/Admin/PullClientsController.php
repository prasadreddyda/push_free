<?php

namespace Modules\Finaltest\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Finaltest\Entities\PullClients;
use Modules\Finaltest\Http\Requests\CreatePullClientsRequest;
use Modules\Finaltest\Http\Requests\UpdatePullClientsRequest;
use Modules\Finaltest\Repositories\PullClientsRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class PullClientsController extends AdminBaseController
{
    /**
     * @var PullClientsRepository
     */
    private $pullclients;

    public function __construct(PullClientsRepository $pullclients)
    {
        parent::__construct();

        $this->pullclients = $pullclients;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$pullclients = $this->pullclients->all();

        return view('finaltest::admin.pullclients.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('finaltest::admin.pullclients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePullClientsRequest $request
     * @return Response
     */
    public function store(CreatePullClientsRequest $request)
    {
        $this->pullclients->create($request->all());

        return redirect()->route('admin.finaltest.pullclients.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('finaltest::pullclients.title.pullclients')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  PullClients $pullclients
     * @return Response
     */
    public function edit(PullClients $pullclients)
    {
        return view('finaltest::admin.pullclients.edit', compact('pullclients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PullClients $pullclients
     * @param  UpdatePullClientsRequest $request
     * @return Response
     */
    public function update(PullClients $pullclients, UpdatePullClientsRequest $request)
    {
        $this->pullclients->update($pullclients, $request->all());

        return redirect()->route('admin.finaltest.pullclients.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('finaltest::pullclients.title.pullclients')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  PullClients $pullclients
     * @return Response
     */
    public function destroy(PullClients $pullclients)
    {
        $this->pullclients->destroy($pullclients);

        return redirect()->route('admin.finaltest.pullclients.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('finaltest::pullclients.title.pullclients')]));
    }
}
