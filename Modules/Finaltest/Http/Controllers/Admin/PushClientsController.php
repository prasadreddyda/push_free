<?php

namespace Modules\Finaltest\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Finaltest\Entities\PushClients;
use Modules\Finaltest\Http\Requests\CreatePushClientsRequest;
use Modules\Finaltest\Http\Requests\UpdatePushClientsRequest;
use Modules\Finaltest\Repositories\PushClientsRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class PushClientsController extends AdminBaseController
{
    /**
     * @var PushClientsRepository
     */
    private $pushclients;

    public function __construct(PushClientsRepository $pushclients)
    {
        parent::__construct();

        $this->pushclients = $pushclients;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$pushclients = $this->pushclients->all();

        return view('finaltest::admin.pushclients.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('finaltest::admin.pushclients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePushClientsRequest $request
     * @return Response
     */
    public function store(CreatePushClientsRequest $request)
    {
        $this->pushclients->create($request->all());

        return redirect()->route('admin.finaltest.pushclients.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('finaltest::pushclients.title.pushclients')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  PushClients $pushclients
     * @return Response
     */
    public function edit(PushClients $pushclients)
    {
        return view('finaltest::admin.pushclients.edit', compact('pushclients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PushClients $pushclients
     * @param  UpdatePushClientsRequest $request
     * @return Response
     */
    public function update(PushClients $pushclients, UpdatePushClientsRequest $request)
    {
        $this->pushclients->update($pushclients, $request->all());

        return redirect()->route('admin.finaltest.pushclients.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('finaltest::pushclients.title.pushclients')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  PushClients $pushclients
     * @return Response
     */
    public function destroy(PushClients $pushclients)
    {
        $this->pushclients->destroy($pushclients);

        return redirect()->route('admin.finaltest.pushclients.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('finaltest::pushclients.title.pushclients')]));
    }
}
