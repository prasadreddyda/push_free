<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/finaltest'], function (Router $router) {
    $router->bind('pushclients', function ($id) {
        return app('Modules\Finaltest\Repositories\PushClientsRepository')->find($id);
    });
    $router->get('pushclients', [
        'as' => 'admin.finaltest.pushclients.index',
        'uses' => 'PushClientsController@index',
        'middleware' => 'can:finaltest.pushclients.index'
    ]);
    $router->get('pushclients/create', [
        'as' => 'admin.finaltest.pushclients.create',
        'uses' => 'PushClientsController@create',
        'middleware' => 'can:finaltest.pushclients.create'
    ]);
    $router->post('pushclients', [
        'as' => 'admin.finaltest.pushclients.store',
        'uses' => 'PushClientsController@store',
        'middleware' => 'can:finaltest.pushclients.create'
    ]);
    $router->get('pushclients/{pushclients}/edit', [
        'as' => 'admin.finaltest.pushclients.edit',
        'uses' => 'PushClientsController@edit',
        'middleware' => 'can:finaltest.pushclients.edit'
    ]);
    $router->put('pushclients/{pushclients}', [
        'as' => 'admin.finaltest.pushclients.update',
        'uses' => 'PushClientsController@update',
        'middleware' => 'can:finaltest.pushclients.edit'
    ]);
    $router->delete('pushclients/{pushclients}', [
        'as' => 'admin.finaltest.pushclients.destroy',
        'uses' => 'PushClientsController@destroy',
        'middleware' => 'can:finaltest.pushclients.destroy'
    ]);
    $router->bind('pullclients', function ($id) {
        return app('Modules\Finaltest\Repositories\PullClientsRepository')->find($id);
    });
    $router->get('pullclients', [
        'as' => 'admin.finaltest.pullclients.index',
        'uses' => 'PullClientsController@index',
        'middleware' => 'can:finaltest.pullclients.index'
    ]);
    $router->get('pullclients/create', [
        'as' => 'admin.finaltest.pullclients.create',
        'uses' => 'PullClientsController@create',
        'middleware' => 'can:finaltest.pullclients.create'
    ]);
    $router->post('pullclients', [
        'as' => 'admin.finaltest.pullclients.store',
        'uses' => 'PullClientsController@store',
        'middleware' => 'can:finaltest.pullclients.create'
    ]);
    $router->get('pullclients/{pullclients}/edit', [
        'as' => 'admin.finaltest.pullclients.edit',
        'uses' => 'PullClientsController@edit',
        'middleware' => 'can:finaltest.pullclients.edit'
    ]);
    $router->put('pullclients/{pullclients}', [
        'as' => 'admin.finaltest.pullclients.update',
        'uses' => 'PullClientsController@update',
        'middleware' => 'can:finaltest.pullclients.edit'
    ]);
    $router->delete('pullclients/{pullclients}', [
        'as' => 'admin.finaltest.pullclients.destroy',
        'uses' => 'PullClientsController@destroy',
        'middleware' => 'can:finaltest.pullclients.destroy'
    ]);
// append


});
