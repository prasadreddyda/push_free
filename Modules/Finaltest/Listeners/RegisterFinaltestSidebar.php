<?php

namespace Modules\Finaltest\Listeners;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Events\BuildingSidebar;
use Modules\User\Contracts\Authentication;

class RegisterFinaltestSidebar implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    public function handle(BuildingSidebar $sidebar)
    {
        $sidebar->add($this->extendWith($sidebar->getMenu()));
    }

    /**
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('finaltest::finaltests.title.finaltests'), function (Item $item) {
                $item->icon('fa fa-copy');
                $item->weight(10);
                $item->authorize(
                     /* append */
                );
                $item->item(trans('finaltest::pushclients.title.pushclients'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.finaltest.pushclients.create');
                    $item->route('admin.finaltest.pushclients.index');
                    $item->authorize(
                        $this->auth->hasAccess('finaltest.pushclients.index')
                    );
                });
                $item->item(trans('finaltest::pullclients.title.pullclients'), function (Item $item) {
                    $item->icon('fa fa-copy');
                    $item->weight(0);
                    $item->append('admin.finaltest.pullclients.create');
                    $item->route('admin.finaltest.pullclients.index');
                    $item->authorize(
                        $this->auth->hasAccess('finaltest.pullclients.index')
                    );
                });
// append


            });
        });

        return $menu;
    }
}
